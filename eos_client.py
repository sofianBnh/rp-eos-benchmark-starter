import argparse
import csv
import datetime
import json
import logging
import os
import subprocess
import sys
import time
from json import JSONDecodeError
from os import path
from random import randint

from lib.common import init_logger, AbstractClient, gen_key_pair, DEFAULT_MONITORING_FOLDER

LOGGER = logging.getLogger(__name__)

WALLET_ADDRESS = '127.0.0.1:8899'
CLIENT_ACCOUNT = 'client'


def _gen_kv_action():
    key = randint(0, 5000)
    value = randint(0, 50)
    return [
        {'action': 'put',
         'payload': {
             'key': key,
             'value': value
         }},
        {'action': 'get',
         'payload': {
             'key': key,
         }}
    ]


def _gen_sb_action():
    return [
        {'action': 'send',
         'payload': {
             'from': 'raven',
             'to': 'ivan',
             'amount': randint(0, 50000)
         }},
        {'action': 'send',
         'payload': {
             'from': 'ivan',
             'to': 'raven',
             'amount': randint(0, 50000)
         }}
    ]


INIT_TX = {
    'small-bank': lambda: [
        {'action': 'fill',
         'payload': {
             'user': 'raven'
         }},
        {'action': 'fill',
         'payload': {
             'user': 'ivan'
         }}
    ]
}

LOAD_GENERATORS = {
    'do-nothing': lambda: [{
        'action': 'noop',
        'payload': {}
    }],
    'cpu-heavy': lambda: [{
        'action': 'launch',
        'payload': {
            'size': 600
        }
    }],
    'kv-store': _gen_kv_action,
    'small-bank': _gen_sb_action,
}


class EOSClient(AbstractClient):
    def __init__(self, server_node_url, load, benchmark, node_count):
        super().__init__(server_node_url, load, benchmark)
        key_pair = gen_key_pair()
        self._public = key_pair['public']
        self._cleos('wallet import --private-key {}'.format(key_pair['private']))
        self._cleos('create account dev {} {}'.format(CLIENT_ACCOUNT, self._public))

        self._load_txn = []

        date = datetime.date.today()
        logging_file_name = '{}-{}-l_{}-n_{}-benchmark-requests.csv'.format(date, benchmark, load, node_count)
        self._tx_state_log_file = path.join(DEFAULT_MONITORING_FOLDER, logging_file_name)

    def _cleos(self, arguments):
        os.system('cleos --wallet-url http://{} --url http://{} {}'
                  .format(WALLET_ADDRESS, self._server_node_url, arguments))

    def generate_load(self):

        if self._benchmark in INIT_TX.keys():
            for tx in INIT_TX[self._benchmark]():
                self._load_txn.append(tx)

        while len(self._load_txn) < self._load:
            for tx in LOAD_GENERATORS[self._benchmark]():
                self._load_txn.append(tx)

    def start_load(self):
        if not os.path.exists(DEFAULT_MONITORING_FOLDER):
            LOGGER.info('Logs directory not found, creating "{}"'.format(DEFAULT_MONITORING_FOLDER))
            os.makedirs(DEFAULT_MONITORING_FOLDER)

        with open(self._tx_state_log_file, 'w+', newline='') as tx_log:
            writer = csv.writer(tx_log)
            writer.writerow(['time', 'status'])
            writer.writerow([time.time(), 'start'])

            for tx in self._load_txn:
                response = self._send_transaction(tx)

                if response:
                    status = response['processed']['receipt']['status']
                else:
                    status = 'fail'

                LOGGER.debug("Transaction State {}".format(status))

                writer.writerow([
                    time.time(),
                    status
                ])

        LOGGER.debug('Finished the submission of events')

    def _send_transaction(self, tx) -> dict:

        benchmark_account = str(self._benchmark).replace('-', '')

        cleos_out = subprocess.Popen(
            ['cleos', '--url', 'http://{}'.format(self._server_node_url),
             'push', 'action', benchmark_account,
             tx['action'], json.dumps(tx['payload']), '-f',
             '-p', '{}@active'.format(CLIENT_ACCOUNT), '-j'],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT
        ).communicate()[0].decode('utf-8')

        try:
            result = json.loads(cleos_out)
        except JSONDecodeError:
            LOGGER.error(cleos_out)
            result = None

        return result


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=3,
                        help='Increase level of output sent to stderr')

    parser.add_argument('-l', '--load',
                        help='Number of transactions to send',
                        default=4)

    parser.add_argument('-c', '--node-count',
                        help='Number of Node running ',
                        default=4)

    parser.add_argument('-b', '--benchmark',
                        help='Name of the benchmark',
                        required=True)

    parser.add_argument('-s', '--server',
                        help='IP or domain of the rest api of a server',
                        default='localhost:8888')

    return parser.parse_args(args)


def main():
    try:
        opts = parse_args(sys.argv[1:])
        init_logger(opts.verbose)

        load = int(opts.load) or 10
        benchmark = opts.benchmark
        server = opts.server
        node_count = opts.node_count

        client = EOSClient(
            server_node_url=server,
            load=load,
            benchmark=benchmark,
            node_count=node_count
        )

        client.generate_load()

        client.start_load()
    except KeyboardInterrupt:
        sys.exit(0)


if __name__ == '__main__':
    main()
