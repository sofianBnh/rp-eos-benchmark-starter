#!/usr/bin/env bash

sudo apt update
curl -L https://github.com/EOSIO/eos/releases/download/v1.5.3/eosio_1.5.3-1-ubuntu-16.04_amd64.deb \
     -o /tmp/eosio_1.5.3-1-ubuntu-16.04_amd64.deb
sudo apt install -y /tmp/eosio_1.5.3-1-ubuntu-16.04_amd64.deb
