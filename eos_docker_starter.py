import argparse
import json
import sys
from os.path import join

from lib.common import *
from lib.start_keosd import set_up_nodes

LOGGER = logging.getLogger(__name__)
TEMPLATE_NODE_PRIVATE_KEY = 'PRIVKEY'
TEMPLATE_NODE_PUBLIC_KEY = 'PUBKEY'
KEYS_DIR = './keys'


def _prepare_keys(count: int) -> list:
    key_store = []

    LOGGER.info('Preparing Keys for Nodes')

    if not os.path.exists(KEYS_DIR):
        LOGGER.info('Keys directory not found, creating "{}"'.format(KEYS_DIR))
        os.makedirs(KEYS_DIR)

    for i in range(count):
        key_pair = gen_key_pair()

        key_store.append(key_pair)

        key_file_name = 'keys-nodeos{}.txt'.format(custom_counter(i + 1))
        key_file_path = join(KEYS_DIR, key_file_name)

        with open(key_file_path, 'w+') as key_file:
            LOGGER.debug('Writing keys to {}'.format(key_file_path))
            key_file.write(json.dumps(key_pair))

    return key_store


def _generate_docker_compose(key_store: list, index: int, node_template, network):
    with open(node_template, 'r') as template_file:
        template = template_file.read()

        private = key_store[index]['private']
        public = key_store[index]['public']

        node_file_content = template \
            .replace(TEMPLATE_NODE_ID, custom_counter(index + 1)) \
            .replace(TEMPLATE_NODE_NETWORK_NAME, network) \
            .replace(TEMPLATE_NODE_PRIVATE_KEY, private) \
            .replace(TEMPLATE_NODE_PUBLIC_KEY, public)

        base, ext = node_template.split('.')
        node_file_name = base + '-' + str(index) + '.' + ext

        with open(node_file_name, 'w+') as node_file:
            node_file.write(node_file_content)

        return node_file_name


def _docker_up(docker_compose_file: str):
    LOGGER.info('Starting the node {}'.format(docker_compose_file))
    os.system('docker-compose -f {} up -d --build'.format(docker_compose_file))


class DockerStarter:
    def __init__(self, node_count, node_template, boot_node=None):
        self._node_count = int(node_count)
        self._node_template = node_template
        self._boot_node = boot_node
        self._services = {}

    def start_services(self):
        if self._boot_node is not None:
            _docker_up(self._boot_node)

        for service, docker_file in self._services.items():
            LOGGER.info('Starting the {}'.format(service))
            _docker_up(docker_file)

    def generate_nodes(self):
        LOGGER.info('Generating docker eos-compose file for {} nodes'.format(self._node_count))

        key_store = _prepare_keys(self._node_count)

        network = get_network_base_name(self._boot_node)

        for i in range(self._node_count):
            file = _generate_docker_compose(key_store, i, self._node_template, network)
            self._services['nodeos-{}'.format(custom_counter(i + 1))] = file


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=0,
                        help='Increase level of output sent to stderr')

    parser.add_argument('-c', '--node-count',
                        help='Number of nodes in the network',
                        default=4)

    parser.add_argument('-t', '--node-template',
                        help='Docker eos-compose template of the standard nodes',
                        required=True)

    parser.add_argument('-b', '--boot-node',
                        help='Docker eos-compose of the Boot',
                        required=True)

    return parser.parse_args(args)


def main():
    try:
        opts = parse_args(sys.argv[1:])
        init_logger(opts.verbose)

        node_count = int(opts.node_count)
        node_template = opts.node_template
        boot_node = opts.boot_node

        docker_stater = DockerStarter(
            node_count=node_count,
            node_template=node_template,
            boot_node=boot_node,
        )

        docker_stater.generate_nodes()
        docker_stater.start_services()

        set_up_nodes(node_count)

    except KeyboardInterrupt:
        sys.exit(0)


if __name__ == '__main__':
    main()
