import logging
import os
import subprocess
from abc import ABC, abstractmethod

DEFAULT_MONITORING_FOLDER = 'logs'
TEMPLATE_NODE_ID = 'TAG'
TEMPLATE_NODE_NETWORK_NAME = 'NET'
EVENT_SUFFIX = 'event'
LOGGER = logging.getLogger(__name__)


class SubscriberError(Exception):
    pass


def gen_key_pair():
    cleos_out = str(subprocess.Popen(
        ['cleos', 'create', 'key', '--to-console'],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    ).communicate()[0].decode('utf-8'))
    LOGGER.debug('Received keys:\n{}'.format(cleos_out))
    keys = cleos_out.split('\n')
    key_pair = {
        'private': keys[0].split(':')[1].strip(),
        'public': keys[1].split(':')[1].strip(),
    }
    return key_pair


def custom_counter(num):
    return ((num == 0) and "0") or (
            custom_counter(num // 5).strip("0") + "12345"[:5][num % 5])


def get_network_base_name(boot_node_file):
    docker_compose_directory = os.path.dirname(boot_node_file)
    if docker_compose_directory == '':
        docker_compose_directory = os.path.basename(os.getcwd())
    return docker_compose_directory


class AbstractClient(ABC):
    """
    Abstract class for Blockchain Client
    """

    @abstractmethod
    def __init__(self, server_node_url, load, benchmark):
        self._server_node_url = server_node_url
        self._load = int(load)
        self._benchmark = benchmark

    @abstractmethod
    def generate_load(self):
        pass

    @abstractmethod
    def start_load(self):
        pass


def init_logger(level):
    logger = logging.getLogger()

    handler = logging.StreamHandler()

    formatter = logging.Formatter(
        fmt='[ %(asctime)s ] :: %(levelname)5s :: ( %(module)s ) -- %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    if level == 1:
        logger.setLevel(logging.INFO)
    elif level > 1:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.WARN)
