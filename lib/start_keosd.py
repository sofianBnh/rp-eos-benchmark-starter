#!/bin/python3

import json
import logging
import os
import time
from os import system
from os.path import join
from threading import Thread

from lib.common import custom_counter, gen_key_pair

KEYS_DIR = './keys'
CONTRACTS_DIR = './contracts'

EOS_PRIVATE_KEY = '5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3'

WALLET_ADDRESS = '127.0.0.1:8899'
GENESIS_ADDRESS = 'localhost:8888'

CONTRACTS = ['donothing', 'cpuheavy', 'kvstore', 'smallbank']
BIOS_CONTRACT = 'eosio.bios'
DEV_KEY_FILE = 'keys-dev.txt'

LOGGER = logging.getLogger(__name__)


def _cleos(arguments):
    command = 'cleos --wallet-url http://{} {}'.format(WALLET_ADDRESS, arguments)
    LOGGER.debug('Executing:\n{}'.format(command))
    system(command)


def _cleos_node_ctl(arguments):
    _cleos('--url http://{} {}'.format(GENESIS_ADDRESS, arguments))


def _start_keosd():
    keosd_t = Thread(target=system, args=['keosd --http-server-address {}'
                     .format(WALLET_ADDRESS)])
    keosd_t.start()
    time.sleep(2)  # Waiting for the server to start
    return keosd_t


def _deploy_custom_contracts():
    key_pair = gen_key_pair()

    private = key_pair['private']
    public = key_pair['public']

    _cleos('wallet import --private-key {}'.format(private))

    _cleos_node_ctl('create account eosio dev {}'.format(public))

    for contract in CONTRACTS:
        _cleos_node_ctl('create account eosio {} {}'.format(contract, public))

        _cleos_node_ctl('set account permission {} active --add-code'
                        .format(contract))

        contract_dir = join(CONTRACTS_DIR, contract)
        _cleos_node_ctl('set contract {} {} -p {}@active'
                        .format(contract, contract_dir, contract))


def _set_nodes_accounts(count) -> list:
    nodes = []

    for i in range(count):
        node_name = 'nodeos{}'.format(custom_counter(i + 1))
        key_file = 'keys-{}.txt'.format(node_name)
        key_file_path = join(KEYS_DIR, key_file)

        with open(key_file_path, 'r') as key_file:
            pair = json.loads(key_file.read())
            private, public = pair['private'], pair['public']

            _cleos('wallet import --private-key {}'.format(private))

            _cleos_node_ctl('create account eosio {} {}'
                            .format(node_name, public))

        nodes.append({
            'producer_name': node_name,
            'block_signing_key': public
        })

    return nodes


def _set_all_as_prod(nodes):
    schema = {"schedule": nodes}

    schema = json.dumps(schema)
    _cleos_node_ctl("push action eosio setprods '{}' -p eosio@active".format(schema))


def set_up_nodes(count: int):
    keosd_t = _start_keosd()

    LOGGER.debug("Working from {}".format(os.getcwd()))

    _cleos('wallet create --file default-password.txt')

    _cleos('wallet import --private-key {}'.format(EOS_PRIVATE_KEY))

    bios_contract_dir = join(CONTRACTS_DIR, BIOS_CONTRACT)
    _cleos_node_ctl('set contract eosio {}'.format(bios_contract_dir))

    nodes = _set_nodes_accounts(count)

    _deploy_custom_contracts()

    _set_all_as_prod(nodes)

    keosd_t.join()
