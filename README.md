# Benchmarking EOS

This repository has the scripts used to test and deploy the EOS Distributed Ledger Technology Platform. 
In it, one can find the automated deployment script for quickly having a network running on docker. The script
uses docker and docker-compose and generates a docker-compose file to start the network of nodes. Each node is composed of a container based on ubuntu with an installation of EOS via the official deb.


**Disclaimer**: This is benchmark does not use some of the key optimization features of eos. 

## Sources

For making this project, the following sources were used:

- [Domonit](https://github.com/eon01/DoMonit/tree/master/domonit): Monitoring Docker Container, was ported to python3 and had some bug fixes.
- [EOS Docs](https://developers.eos.io/): Official Documentation for the base of the docker-compose files.
- [Blockbench](https://github.com/ooibc88/blockbench): For the example, smart contracts (Transaction Processors) used to test the performance.
- [SEN](https://github.com/TomasTomecek/sen/tree/master/sen): Command Line Monitoring of Docker, used the CPU percentage calculation function.

## Files

The repository is composed of the following files and directories:

```
    .
    ├── contracts                      # Example Smart Contracts, some of them were used during the benchmark
    ├── dev                            # Contains development utilities 
    ├── docker                         # Contains the Dockerfiles and contexts for the deployment
    ├── eos-compose                    # Contains the docker compose bases for the nodes and the genesis
    ├── docker_monitor.py              # Script for monitoring the docker containers
    ├── eos_docker_starter.py          # Script for starting the nodes of the network
    ├── lib                            # Python modules used in the main scripts
    ├── logs                           # Folder for holding the logs of the main scripts
    ├── README.md                      # This file
    ├── requirements.txt               # Python packages required for running the scripts
    └── eos_client.py                  # Script for sending transactions to nodeos
```

## Starting the Benchmark

For benchmarking the eos network, we used a script for:

- Deploying the network.
- Sending the transactions.
- Receiving the confirmation events.
- Monitoring the usage of the containers.

## Starting the Nodes

**Purpose:**

Starting the nodes of the network using docker ad docker compose.

**Help:**

```
    usage: eos_docker_starter.py [-h] [-v] [-c NODE_COUNT] -t NODE_TEMPLATE -b
                                BOOT_NODE

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -c NODE_COUNT, --node-count NODE_COUNT
                            Number of nodes in the network
    -t NODE_TEMPLATE, --node-template NODE_TEMPLATE
                            Docker eos-compose template of the standard nodes
    -b BOOT_NODE, --boot-node BOOT_NODE
                            Docker eos-compose of the Boot
```

## Starting the Monitoring

**Purpose:**

Monitor the containers that are being used by the 

**Help:**

```
    usage: docker_monitor.py [-h] [-v] [-i INTERVAL] [-s SERVICES [SERVICES ...]]
                            [-b BENCHMARK]

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -i INTERVAL, --interval INTERVAL
                            Interval of measures in seconds
    -s SERVICES [SERVICES ...], --services SERVICES [SERVICES ...]
                            Container names of the services to monitor separated by spaces
    -b BENCHMARK, --benchmark BENCHMARK
                            Name of the test
```

**Example:**

```bash
python3 docker_monitor.py -b do-nothing
```

This command will start monitoring ALL the containers that are up and will save the results in a 
file `logs/date-of-the-day-do-nothing-benchmark-results.csv`.


## Starting the Client

**Purpose:**

Send the requests to Nodeos

**Help:**

```
    usage: eos_client.py [-h] [-v] [-l LOAD] [-c NODE_COUNT] -b BENCHMARK
                        [-s SERVER]

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -l LOAD, --load LOAD  Number of transactions to send
    -c NODE_COUNT, --node-count NODE_COUNT
                            Number of Node running
    -b BENCHMARK, --benchmark BENCHMARK
                            Name of the benchmark
    -s SERVER, --server SERVER
                            IP or domain of the rest api of a server
```

**Example:**

```bash
python3 eos_client.py -l 10 -s localhost -b do-nothing
```