#!/bin/bash

set -xe

nodeos \
    --producer-name $PRODUCER_NAME \
    --plugin eosio::chain_api_plugin \
    --plugin eosio::net_api_plugin \
    --http-server-address 127.0.0.1:8888 \
    --p2p-listen-endpoint 127.0.0.1:9876 \
    --p2p-peer-address $GENESIS_NODE:9876 \
    --private-key "[\"$PUBLIC\",\"$PRIVATE\"]"
