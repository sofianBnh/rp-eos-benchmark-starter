#!/bin/bash

set -xe

nodeos \
    --enable-stale-production \
    --producer-name eosio \
    --plugin eosio::chain_api_plugin \
    --plugin eosio::net_api_plugin \
    --http-server-address 0.0.0.0:8888 \
    --http-alias nodeos-genesis:8888 \
    --http-alias localhost:8888
