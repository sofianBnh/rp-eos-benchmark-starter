#include <eosiolib/eosio.hpp>

using namespace eosio;

class donothing : public contract {
public:
    using contract::contract;

    [[eosio::action]] void noop() {
        print("Executing Call for Do Nothing");
        action(
                permission_level{_self, "active"_n},
                _self, "report"_n,
                std::make_tuple("done")
        ).send();
    }
};

EOSIO_DISPATCH(donothing, (noop))