#include <eosiolib/eosio.hpp>

using namespace eosio;

class cpuheavy : public contract {
public:
    using contract::contract;

    [[eosio::action]] void launch(int size) {
        int *arr = new int[size];
        fill_arr(arr, size);
        bubble_sort(arr, size);
        action(
                permission_level{_self, "active"_n},
                _self, "report"_n,
                std::make_tuple("done")
        ).send();
    }

private:

    void fill_arr(int *arr, int size) {
        for (int i = 0; i < size; ++i) {
            arr[i] = size - i;
        }
    }

    void swap(int *xp, int *yp) {
        int temp = *xp;
        *xp = *yp;
        *yp = temp;
    }

    void bubble_sort(int *arr, int n) {
        int i, j;
        for (i = 0; i < n - 1; i++)
            for (j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1])
                    swap(&arr[j], &arr[j + 1]);
    }
};

EOSIO_DISPATCH(cpuheavy, (launch)
)