#include <eosiolib/eosio.hpp>

using namespace eosio;

class smallbank : public contract {
public:
    using contract::contract;

    [[eosio::action]] void fill(name user) {

        bank spbank(_code, _code.value);
        _set_balance(user, 10000000);

        action(
                permission_level{_self, "active"_n},
                _self, "report"_n,
                std::make_tuple("done")
        ).send();

    }

    [[eosio::action]] void send(name from, name to, int amount) {


        bank spbank(_code, _code.value);

        auto iter_from = spbank.find(from.value);

        auto iter_to = spbank.find(to.value);


        if (iter_from != spbank.end() && iter_to != spbank.end()) {

            if (iter_from->balance >= amount) {

                int temp_from = iter_from->balance - amount;
                int temp_to = iter_to->balance + amount;

                _set_balance(from, temp_from);
                _set_balance(to, temp_to);

                action(
                        permission_level{_self, "active"_n},
                        _self, "report"_n,
                        std::make_tuple("done")
                ).send();
            } else {
                action(
                        permission_level{_self, "active"_n},
                        _self, "notify"_n,
                        std::make_tuple("HAAAAHHAAAAA !!!!")
                ).send();
            }
        } else {
            action(
                    permission_level{_self, "active"_n},
                    _self, "notify"_n,
                    std::make_tuple("Nooooooooooooo")
            ).send();
        }


    }

private:

    void _set_balance(name user, int amount) {
        bank spbank(_code, _code.value);
        auto iterator = spbank.find(user.value);

        if (iterator == spbank.end()) {
            spbank.emplace(_self, [&](auto &s) {
                s.user = user;
                s.balance = amount;
            });
        } else {
            spbank.modify(iterator, _self, [&](auto &s) {
                s.user = user;
                s.balance = amount;
            });
        }
    }

    struct [[eosio::table]] account {
        name user;
        int balance;

        uint64_t primary_key() const { return user.value; }
    };

    typedef eosio::multi_index<"accounts"_n, account> bank;

};

EOSIO_DISPATCH(smallbank, (fill)(send)
)