#include <eosiolib/eosio.hpp>
#include <string>

using namespace eosio;

class kvstore : public contract {
public:

    using contract::contract;

    [[eosio::action]] void get(int key) {

        key_value_store store(_code, _code.value);

        auto iterator = store.find((int64_t) key);

        action(
                permission_level{_self, "active"_n},
                _self, "report"_n,
                std::make_tuple(iterator->value)
        ).send();
    }

    [[eosio::action]] void put(int key, std::string value) {

        key_value_store store(_code, _code.value);

        auto iterator = store.find((int64_t) key);

        if (iterator == store.end()) {
            store.emplace(_self, [&](auto &s) {
                s.key = (int64_t) key;
                s.value = value;
            });
        } else {
            store.modify(iterator, _self, [&](auto &s) {
                s.key = (int64_t) key;
                s.value = value;
            });
        }

        int64_t real_key = (int64_t) key;

        action(
                permission_level{_self, "active"_n},
                _self, "report"_n,
                std::make_tuple(value)
        ).send();
    }


private:
    struct [[eosio::table]] pair {
        uint64_t key;
        std::string value;

        uint64_t primary_key() const { return key; }
    };

    typedef eosio::multi_index<"pairs"_n, pair> key_value_store;

};

EOSIO_DISPATCH(kvstore, (put)(get)
)